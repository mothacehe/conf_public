(let ((frmonpc1176 (build-machine
                    (name "10.10.10.3")
                    (systems (list "x86_64-linux"))
                    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIMAMwjQSTaCZIIo0ow32Mh3MSymbo7+or0C8rK2jgx8")
                    (user "mathieu")
                    (private-key "/home/mathieu/.ssh/id_rsa")))
      (frmonpc1177 (build-machine
                    (name "10.10.10.9")
                    (systems (list "x86_64-linux"))
                    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPyXDdaV1Zm3pVIBQncnZ3Yu8YTrgPWGw6TJ6JaNQHTM")
                    (user "mathieu")
                    (private-key "/home/mathieu/.ssh/id_rsa"))))
  (list frmonpc1176 frmonpc1177))
