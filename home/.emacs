(setq auto-save-default nil
      backup-inhibited t
      column-number-mode t
      compilation-scroll-output t
      esup-user-init-file (file-truename "~/.emacs")
      find-file-visit-truename t
      inhibit-startup-message t
      lisp-path (expand-file-name "~/.emacs.d/lisp/")
      custom-file "~/.emacs.d/my-custom.el"
      vc-follow-symlinks t
      visible-bell 1
      truncate-lines t
      shell-file-name "bash"
      uniquify-buffer-name-style 'reverse
      ad-redefinition-action 'accept
      ggtags-oversize-limit 0
      ggtags-update-on-save t
      calendar-week-start-day 1
      select-enable-primary t
      select-enable-clipboard nil
      gc-cons-threshold 100000000
      dired-dwim-target t
      user-full-name "Mathieu Othacehe"
      enable-remote-dir-locals t
      auth-sources '("~/pass/home/netrc.gpg"))

(setq-default indent-tabs-mode nil
              fill-column 78)

(defalias 'yes-or-no-p 'y-or-n-p)

(global-auto-revert-mode)
(global-font-lock-mode t)
(global-page-break-lines-mode t)
(show-paren-mode)
(blink-cursor-mode -1)
(yas-global-mode)
(load custom-file)
(evil-mode)
(ivy-mode)
(counsel-mode)
(savehist-mode)

(require 'iso-transl)                   ;fix undefined dead keys

(defun gui-theme ()
  (set-face-attribute 'default nil :height 125)
  (load-theme 'doom-one-light t)
  (menu-bar-mode -1)
  (toggle-scroll-bar -1)
  (tool-bar-mode -1))

(defun term-theme ()
  (menu-bar-mode -1)
  (load-theme 'tango-dark t))

(if (display-graphic-p)
    (gui-theme)
  (term-theme))

(global-set-key (kbd "C-c s") 'my-rg)
(global-set-key (kbd "C-c g") 'magit-status)
(global-set-key (kbd "C-c r") 'revert-buffer)

(global-set-key (kbd "C-x m") 'gnus)
(global-set-key (kbd "C-s") 'swiper)

(add-to-list 'auto-mode-alist '("\\.html$" . web-mode))

(with-eval-after-load 'rg
  (setq rg-command-line-flags '("--hidden" "-L" "-g !*.git"))
  (rg-define-search my-rg :files "everything"))

(with-eval-after-load 'cc-vars
  (setf (cdr (assoc 'other c-default-style)) "linux")
  (add-hook 'c-mode-common-hook 'whitespace-mode)
  (add-hook 'c-mode-common-hook 'dtrt-indent-mode)
  (add-hook 'c-mode-common-hook (lambda () (setq indent-tabs-mode t)))
  (add-hook 'c-mode-hook 'ggtags-mode)
  (add-hook 'c++-mode-hook 'google-set-c-style))

(with-eval-after-load 'dired
  (dired-async-mode 1)
  (setq dired-recursive-deletes 'always
        dired-listing-switches "-lha"))

(with-eval-after-load 'ediff
  (setq ediff-split-window-function 'split-window-horizontally
        ediff-window-setup-function 'ediff-setup-windows-plain))

(with-eval-after-load 'elisp-mode
  (add-hook 'emacs-lisp-mode-hook 'whitespace-mode)
  (add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode))

(with-eval-after-load 'erc
  (setq erc-autojoin-channels-alist '((".*" "#guix"))
        erc-default-server "irc.libera.chat"
        erc-nick "mothacehe"
        erc-prompt-for-password nil
        erc-server-auto-reconnect nil
        erc-track-exclude-server-buffer t
        erc-track-exclude-types '("JOIN" "PART" "QUIT" "NICK" "MODE")))

(with-eval-after-load 'evil
  (setq evil-want-C-i-jump nil
        evil-symbol-word-search t
        evil-default-state 'emacs
        evil-insert-state-modes nil
        evil-motion-state-modes nil
        evil-move-cursor-back nil
        evil-kill-on-visual-paste nil
        evil-move-cursor-back t)

  (mapc (lambda (mode)
          (evil-set-initial-state mode 'normal))
        '(fundamental-mode prog-mode text-mode conf-mode diff-mode))

  (fset 'evil-visual-update-x-selection 'ignore)

  (setq-default evil-symbol-word-search t)
  (defalias 'forward-evil-word 'forward-evil-symbol)

  (add-hook 'edebug-mode-hook 'evil-normalize-keymaps))

(with-eval-after-load 'flycheck
 (require 'flycheck-google-cpplint)
 (setq-default flycheck-disabled-checkers
               '(c/c++-clang c/c++-gcc)))

(with-eval-after-load 'flyspell
  (require 'auto-dictionary)
  (add-hook 'flyspell-mode-hook 'auto-dictionary-mode))

(with-eval-after-load 'geiser-impl
  (setq geiser-active-implementations '(guile)))

(with-eval-after-load 'git-auto-commit-mode
  (setq-default gac-automatically-push-p t))

(with-eval-after-load 'gnus
  (require 'org)
  (setq gnus-select-method '(nnnil)
        gnus-expert-user t
        gnus-always-read-dribble-file t
        gnus-treat-display-smileys nil
        gnus-summary-display-arrow t
        gnus-large-newsgroup 1000
        gnus-startup-file "~/.gnus/.newsrc"
        nnrss-use-local nil
        gnus-cloud-interactive nil
        gnus-cloud-storage-method nil
        gnus-show-threads nil
        gnus-permanently-visible-groups "nnvirtual"
        gnus-user-date-format-alist
        '(((gnus-seconds-today) . "%H:%M") (t . "%d.%m.%Y %H:%M"))
        gnus-summary-line-format
        (concat "%0{%U%R%} %3{│%}%1{%-16,16&user-date;%} %3{│%}"
                "%4{%-20,20f%}  %3{│%} %1{%B%} %s\n")
        gnus-thread-sort-functions '(gnus-thread-sort-by-most-recent-number)
        gnus-article-sort-functions '((not gnus-article-sort-by-date))
        gnus-message-archive-group '("sent")
        gnus-parameters
        '((".*"
           (posting-style
            (address "othacehe@gnu.org")
            ("X-Message-SMTP-Method" "smtp fencepost.gnu.org 587 othacehe")))
          ("gmail"
           (posting-style
            (address "m.othacehe@gmail.com")
            ("X-Message-SMTP-Method" "smtp smtp.gmail.com 587")
            (gcc nil))))
        gnus-secondary-select-methods
        '((nntp "news.gmane.io")
          (nntp "news.yhetil.org")
          (nnimap "gmail"
                  (nnimap-address "imap.gmail.com")
                  (nnimap-stream ssl))
          (nnmaildir "gnu"
                     (directory "~/mail/gnu")
                     (nnir-notmuch-remove-prefix
                      "/home/mathieu/mail/gnu/"))))
  (add-hook 'gnus-group-mode-hook 'gnus-topic-mode)
  (add-hook 'message-mode-hook 'flyspell-mode))

(with-eval-after-load 'ivy
  (setq ivy-use-virtual-buffers t
        ivy-re-builders-alist '((swiper . ivy--regex-plus)
                                (t . ivy--regex-fuzzy))
        ivy-virtual-abbreviate 'full
        counsel-find-file-ignore-regexp "\\.go\\'"
        enable-recursive-minibuffers t
        recentf-max-saved-items nil))

(with-eval-after-load 'lsp
  (require 'lsp-ui-flycheck)
  (setq lsp-prefer-flymake nil
        lsp-ui-sideline-enable nil)
  (add-hook 'lsp-after-open-hook
            (lambda ()
              (lsp-ui-flycheck-enable 1)))
  (flycheck-add-next-checker 'lsp-ui 'c/c++-googlelint)
  (lsp-register-client
   (make-lsp-client :new-connection
                    (lsp-tramp-connection "pyls")
                    :major-modes '(python-mode)
                    :remote? t
                    :server-id 'pyls-remote)))

(with-eval-after-load 'magit
  (setq magit-bury-buffer-function 'magit-mode-quit-window))

(with-eval-after-load 'message
  (setq message-dont-reply-to-names "othacehe"
        message-cite-reply-position 'below))

(with-eval-after-load 'org
  (require 'org-contacts)
  (require 'org-drill)

  (add-hook 'org-mode-hook 'auto-fill-mode)
  (add-hook 'org-mode-hook 'org-bullets-mode)

  (setq org-capture-templates
        '(("c" "Contacts" entry (file "~/doc/org/contacts.org")
           "* %(org-contacts-template-name)
:PROPERTIES:
:EMAIL: %(org-contacts-template-email)
:END:")))

  (setq org-directory "~/doc/org"
        org-agenda-start-with-log-mode t
        org-default-notes-file "~/doc/org/home.org"
        org-agenda-files '("~/doc/org/")
        org-contacts-files '("~/doc/org/contacts.org")
        org-startup-folded nil
        org-startup-indented nil
        org-src-fontify-natively t
        org-confirm-babel-evaluate nil
        org-adapt-indentation nil))

(with-eval-after-load 'scheme
  (add-hook 'scheme-mode-hook 'whitespace-mode)
  (add-hook 'scheme-mode-hook 'enable-paredit-mode)
  (add-hook 'scheme-mode-hook 'guix-devel-mode)
  (add-hook 'scheme-mode-hook 'yas-minor-mode))

(with-eval-after-load 'sendmail
  (setq send-mail-function 'smtpmail-send-it))

(with-eval-after-load 'simple
  (setq mail-user-agent 'gnus-user-agent))

(with-eval-after-load 'smtpmail
  (setq smtpmail-stream-type 'starttls
        smtpmail-smtp-service 587))

(with-eval-after-load 'with-editor
  (add-hook 'with-editor-mode-hook 'evil-normal-state)
  (add-hook 'with-editor-mode-hook 'yas-minor-mode))

(with-eval-after-load 'whitespace
  (setq whitespace-line-column nil
        whitespace-style '(face trailing lines-tail
                                space-before-tab newline
                                indentation empty space-after-tab)))

(with-eval-after-load 'yasnippet
  (add-to-list 'yas-snippet-dirs "~/guix/etc/snippets")
  (yas-reload-all))


;;;
;;; Gnus apply.
;;;

(defun my-apply-patch-or-abort ()
  (interactive)
  (my-apply-patch-internal "git am || git am --abort"))

(defun my-apply-patch ()
  (interactive)
  (my-apply-patch-internal "git am --reject"))

(defun my-apply-patch-or-abort-attachment (n)
  (interactive "P")
  (my-apply-patch-attachment-internal "git am || git am --abort" n))

(defun my-apply-patch-attachment (n)
  (interactive "P")
  (my-apply-patch-attachment-internal "git am --reject" n))

(defun my-apply-patch-attachment-internal (cmd n)
  "C-u <attachment number> M-x my-apply-..."
  (let ((git-dir "~/guix"))
    (save-window-excursion
      (gnus-article-part-wrapper
       n
       (lambda (handle)
         (let ((default-directory git-dir))
           (mm-pipe-part handle cmd)))))))

(defun my-apply-patch-internal (cmd)
  "Works with a selection of articles."
  (let ((git-dir "~/guix")
        (articles (gnus-summary-work-articles nil)))
    (save-window-excursion
      (while articles
        (gnus-summary-goto-subject (pop articles))
        (with-current-buffer gnus-summary-buffer
          (let ((default-directory git-dir))
            (gnus-summary-save-in-pipe cmd))
          (gnus-article-hide-headers))))))
