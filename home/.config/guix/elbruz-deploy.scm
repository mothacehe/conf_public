(list (machine
       (operating-system (primitive-load "elbruz.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (host-name "elbruz")
                       (system "x86_64-linux")
                       (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHMdTpRKtPxiX971NNSAeZwBbMY+TJRML/hIy5lCod9i")
                       (user "mathieu")
                       (identity "/home/mathieu/.ssh/id_rsa")
                       (port 22)))))
