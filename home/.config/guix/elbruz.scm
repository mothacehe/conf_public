(use-modules (gnu))
(use-package-modules bash cups databases perl ssh)
(use-service-modules avahi cups desktop docker databases networking pm ssh
                     virtualization vpn xorg)

(operating-system
  (locale "en_US.utf8")
  (name-service-switch %mdns-host-lookup-nss)
  (timezone "Europe/Paris")
  (keyboard-layout
   (keyboard-layout "us" "altgr-intl"))
  (host-name "elbruz")
  (users (cons* (user-account
                 (name "mathieu")
                 (comment "Mathieu")
                 (group "users")
                 (home-directory "/home/mathieu")
                 (supplementary-groups
                  '("docker" "wheel" "netdev" "audio" "video" "kvm")))
                (user-account
                 (name "clem")
                 (comment "Clément")
                 (group "users")
                 (home-directory "/home/clem")
                 (supplementary-groups
                  '("docker" "wheel" "netdev" "audio" "video" "kvm")))
                %base-user-accounts))
  (sudoers-file
   (plain-file "sudoers"
               (string-append "root ALL=(ALL) ALL\n"
                              "%wheel ALL=(ALL) NOPASSWD: ALL")))
  (packages
   (append
       (list (specification->package "nss-certs"))
       %base-packages))
  (services
   (append
       (list
        (service avahi-service-type
                 (avahi-configuration (debug? #t)))
        (service cups-service-type
                 (cups-configuration
                  (web-interface? #t)
                  (extensions
                   (list cups-filters))))
        (service dhcp-client-service-type)
        (service docker-service-type)
        (elogind-service)
        (service ntp-service-type)
        (service openssh-service-type
                 (openssh-configuration
                  (openssh openssh-sans-x)
                  (permit-root-login 'without-password)
                  (authorized-keys
                   `(("mathieu"
                      ,(local-file "keys/mathieu.pub"))
                     ("clem"
                      ,(local-file "keys/clem.pub"))))))
        (service qemu-binfmt-service-type
                 (qemu-binfmt-configuration
                  (platforms (lookup-qemu-platforms "arm" "aarch64"))))
        (service special-files-service-type
                 `(("/bin/sh" ,(file-append bash "/bin/sh"))
                   ("/bin/bash" ,(file-append bash "/bin/bash"))
                   ("/usr/bin/perl" ,(file-append perl "/bin/perl"))
                   ("/usr/bin/env" ,(file-append coreutils "/bin/env"))))
        (service wireguard-service-type
                     (wireguard-configuration
                      (addresses (list "10.10.10.5/24"))
                      (peers
                       (list
                        (wireguard-peer
                         (name "elbruz")
                         (endpoint "othacehe.org:443")
                         (public-key "vC91Z9lHP/9RzFdzlXm3qbVMBmtEp5yp/hEwqXTteGw=")
                         (allowed-ips '("10.0.0.0/8"))
                         (keep-alive 25)))))) )
       (modify-services %base-services
         (guix-service-type
          config =>
          (guix-configuration
           (inherit config)
           (discover? #t)
           (substitute-urls
            (append (list "https://substitutes.nonguix.org")
                %default-substitute-urls))
           (authorized-keys
            (append (list
                     (local-file "./keys/meije.scm")
                     (local-file "./keys/nonguix.scm"))
                %default-authorized-guix-keys)))) )))
  (bootloader
   (bootloader-configuration
    (bootloader grub-bootloader)
    (targets (list "/dev/sda"))
    (timeout 5)
    (terminal-inputs '(serial))
    (terminal-outputs '(serial))
    (serial-unit 0)
    (serial-speed 115200)))
  (kernel-arguments '("console=ttyS0,115200"))
  (file-systems
   (cons* (file-system
            (mount-point "/")
            (device
             (uuid "9e5e50ef-9b73-437c-9569-cbea822fb7d7"
                   'ext4))
            (type "ext4"))
          (file-system
            (mount-point "/home")
            (device (file-system-label "home"))
            (type "btrfs")
            (options "compress-force=zstd"))
         %base-file-systems)))
