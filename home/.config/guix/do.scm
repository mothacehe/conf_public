(use-modules (gnu)
             (sysadmin web))
(use-package-modules certs rsync screen ssh web)
(use-service-modules certbot desktop dbus docker networking ssh
                     syncthing sysctl vpn web)

(define (cert-path host file)
  (format #f "/etc/letsencrypt/live/~a/~a.pem" host (symbol->string file)))

(define %nginx-deploy-hook
  (program-file
   "nginx-deploy-hook"
   #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
       (kill pid SIGHUP))))

(operating-system
  (host-name "viso")
  (timezone "Europe/Paris")
  (locale "en_US.utf8")
  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets '("/dev/vda"))
               (terminal-outputs '(console))))
  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device "/dev/vda2")
                         (type "ext4"))
                       (file-system
                         (mount-point "/mnt/volume/")
                         (device
                          (uuid "9e31dd82-1e2f-4c32-8771-0a088e0689ed" 'ext4))
                         (type "ext4")
                         (mount-may-fail? #t))
                       %base-file-systems))
  (packages
   (cons* openssh rsync screen %base-packages))
  (services
   (append
    (list
     (service certbot-service-type
              (certbot-configuration
               (email "othacehe@gnu.org")
               (certificates
                (list
                 (certificate-configuration
                  (domains '("othacehe.org" "ha.othacehe.org"))
                  (deploy-hook %nginx-deploy-hook))
                 (certificate-configuration
                  (domains '("levelocestrigolo.org"))
                  (deploy-hook %nginx-deploy-hook))
                 (certificate-configuration
                  (domains '("root2peak.com"))
                  (deploy-hook %nginx-deploy-hook))))))
     (service dbus-root-service-type)
     (service dhcp-client-service-type)
     (service docker-service-type)
     (service elogind-service-type)
     (service nginx-service-type
              (nginx-configuration
               (server-blocks
                (list
                 ;; othacehe.org
                 (nginx-server-configuration
                  (listen '("443 ssl" "[::]:443 ssl"))
                  (server-name (list "othacehe.org"))
                  (root "/var/www/website/")
                  (locations
                   (list
                    (nginx-location-configuration
                     (uri "/")
                     (body (list "index index.html;")))
                    (nginx-location-configuration
                     (uri "/files")
                     (body (list "alias /var/www/files;")))))
                  (ssl-certificate (cert-path "othacehe.org" 'fullchain))
                  (ssl-certificate-key (cert-path "othacehe.org" 'privkey)))
                 ;; ha.othacehe.org
                 (nginx-server-configuration
                  (listen '("443 ssl" "[::]:443 ssl"))
                  (server-name (list "ha.othacehe.org"))
                  (locations
                   (list
                    (nginx-location-configuration
                     (uri "/")
                     (body
                      (list "proxy_pass http://localhost:8123;"
                            "proxy_set_header Host $host;"
                            "proxy_http_version 1.1;"
                            "proxy_set_header Upgrade $http_upgrade;"
                            "proxy_set_header Connection $connection_upgrade;"
                            )))))
                  (ssl-certificate (cert-path "othacehe.org" 'fullchain))
                  (ssl-certificate-key (cert-path "othacehe.org" 'privkey)))
                 ;; levelocestrigolo.org
                 (nginx-server-configuration
                  (listen '("443 ssl" "[::]:443 ssl"))
                  (server-name (list "levelocestrigolo.org"))
                  (root "/var/www/velorigolo")
                  (locations
                   (list
                    (nginx-location-configuration
                     (uri "/")
                     (body (list "index index.html;")))))
                  (ssl-certificate
                   (cert-path "levelocestrigolo.org" 'fullchain))
                  (ssl-certificate-key
                   (cert-path "levelocestrigolo.org" 'privkey)))
                 ;; root2peak.com
                 (nginx-server-configuration
                  (listen '("443 ssl" "[::]:443 ssl"))
                  (server-name (list "root2peak.com"))
                  (root "/var/www/r2p")
                  (locations
                   (list
                    (nginx-location-configuration
                     (uri "/")
                     (body (list "index index.html;")))
                    (nginx-location-configuration
                     (uri "= /")
                     (body (list "return 302 $index_redirect_uri;")))))
                  (ssl-certificate
                   (cert-path "root2peak.com" 'fullchain))
                  (ssl-certificate-key
                   (cert-path "root2peak.com" 'privkey)))))
              (modules
               (list
                ;; Module to redirect users to the localized pages of their
                ;; choice.
                (file-append nginx-accept-language-module
                  "/etc/nginx/modules/ngx_http_accept_language_module.so")))
              (extra-content
               "\
map $http_upgrade $connection_upgrade {
        default upgrade;
        '' close;
    }
map $http_accept_language $index_redirect_uri {
default \"/en/\";
\"~(^|,)fr\" \"/fr/\";
}")))
     (service oci-container-service-type
              (list
               (oci-container-configuration
                (image "ghcr.io/home-assistant/home-assistant:stable")
                (network "host")
                (volumes
                 '("/mnt/volume/ha:/config")))))
     (service openssh-service-type
              (openssh-configuration
               (openssh openssh-sans-x)
               (permit-root-login 'prohibit-password)
               (authorized-keys
                `(("root"
                   ,(local-file "/home/mathieu/.ssh/id_rsa.pub"))))))
     (service polkit-service-type)
     (service static-web-site-service-type
              (list
               (static-web-site-configuration
                (git-url
                 "https://gitlab.com/mothacehe/website.git")
                (directory "/var/www/website")
                (build-file ".guix.scm"))))
     (service syncthing-service-type
              (syncthing-configuration (user "root")))
     (service wireguard-service-type
              (wireguard-configuration
               (addresses (list "10.10.10.1/24"))
               (table "123")
               (pre-up '("ip rule add iif wg0 table 123 priority 456"))
               (post-down '("ip rule del iif wg0 table 123 priority 456"))
               (port "443")
               (peers
                (list
                 ;; Home
                 (wireguard-peer
                  (name "meije")
                  (public-key "qDa6Fg86tCxiRS8p/7B5butJLYNSALhMZAcx7wuVZ0c=")
                  (allowed-ips '("10.10.10.2/32"))
                  (keep-alive 25))
                 (wireguard-peer
                  (name "elbruz")
                  (public-key "+l7FkW0AtuahYorhfupHmSMawN9XqyMnzQiT6OSjwVA=")
                  (allowed-ips '("10.10.10.5/32"))
                  (keep-alive 25))
                 (wireguard-peer
                  (name "dibona")
                  (public-key "uWQ87FT8LIoaghio7tjPfHb+OhsOkS6tr4wcOg2l5Wo=")
                  (allowed-ips '("10.10.10.4/32"))
                  (keep-alive 25))
                ;; Landis+Gyr
                (wireguard-peer
                  (name "frmonpc1175")
                  (public-key "h3kpqPn1Zh2Oo35T0n7bm43CKWCx1pn/dg+oVqvIYCg=")
                  (allowed-ips '("10.10.10.8/32")))
                 (wireguard-peer
                  (name "frmonpc1176")
                  (public-key "rprbLPXlR/edCVCMJll0LfLdbyU6fONfUK8iAouCunE=")
                  (allowed-ips '("10.10.10.3/32" "10.0.0.0/8")))
                (wireguard-peer
                  (name "frmonpc1177")
                  (public-key "fjYdseJ0mhAj2FKL2JaeR5hlafwhmKUaYMxM6wRPcBM=")
                  (allowed-ips '("10.10.10.9/32"))))))))
    (modify-services %base-services
      (sysctl-service-type
       config =>
       (sysctl-configuration
        (settings (append '(("net.ipv4.ip_forward" . "1"))
                          %default-sysctl-settings))))
      (guix-service-type
       config =>
       (guix-configuration
        (inherit config)
        (authorized-keys
         (append (list (local-file "./keys/meije.scm"))
                 %default-authorized-guix-keys)))) ))))
