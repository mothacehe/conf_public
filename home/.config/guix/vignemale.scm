(use-modules (gnu)
             (gnu image)
             (gnu platforms arm)
             (gnu system images pine64))
(use-package-modules linux)

(define vignemale
  (operating-system
    (inherit pine64-barebones-os)
    (packages (cons i2c-tools %base-packages))))

(image
 (inherit
  (os+platform->image vignemale aarch64-linux
                      #:type pine64-image-type))
 (name 'vignemale-raw-image))
