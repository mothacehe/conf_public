(list (machine
       (operating-system (primitive-load "do.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (host-name "104.248.196.129")
                       (system "x86_64-linux")
                       (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPKk0rIP7as8EbayOz1WCy/DWJks/uCPAn5nSKsn6Hwc")
                       (user "root")
                       (identity "/home/mathieu/.ssh/id_rsa")
                       (port 22)))))
