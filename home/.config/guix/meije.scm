(use-modules (gnu)
             (gnu home)
             (gnu home services desktop)
             (gnu home services mcron)
             (gnu home services shells)
             (daviwil home-services pipewire)
             (nongnu packages linux)
             (nongnu system linux-initrd)
             (srfi srfi-1))
(use-package-modules bash cups databases mail perl radio)
(use-service-modules cups desktop docker databases guix networking pm
                     security-token ssh sound syncthing virtualization
                     vpn xorg)

(define (read-manifest name)
  (with-input-from-file
      (format #f "/home/mathieu/.config/guix/manifests/~a.scm"
              (symbol->string name))
    (lambda ()
      (specifications->packages (read)))))

(define my-home
  (home-environment
   (packages (append-map read-manifest '(base nonfree)))
   (services (list
              (service home-bash-service-type
                       (home-bash-configuration
                        (environment-variables
                         '(("HISTSIZE" . "-1")
                           ("HISTFILESIZE" . "-1")
                           ("PATH" . "$HOME/bin:$PATH")
                           ("PASSWORD_STORE_DIR" . "$HOME/pass")))))
              (service home-mcron-service-type
                       (home-mcron-configuration
                        (jobs
                         (list
                          #~(job
                             '(next-hour (range 0 24 1))
                             "/var/guix/gcroots/getmail")))))
              (service home-dbus-service-type)
              (service home-pipewire-service-type)))))

(operating-system
  (locale "en_US.utf8")
  (name-service-switch %mdns-host-lookup-nss)
  (timezone "Europe/Paris")
  (keyboard-layout
   (keyboard-layout "us" "altgr-intl"))
  (host-name "meije")
  (kernel linux)
  (kernel-arguments '("modprobe.blacklist=dvb_usb_rtl28xxu"))
  (initrd microcode-initrd)
  (firmware (list sof-firmware linux-firmware))
  (users (cons* (user-account
                 (name "mathieu")
                 (comment "Mathieu")
                 (group "users")
                 (home-directory "/home/mathieu")
                 (supplementary-groups
                  '("docker" "wheel" "netdev" "audio" "video" "kvm" "lp"
                    "dialout")))
                %base-user-accounts))
  (sudoers-file
   (plain-file "sudoers"
               (string-append "root ALL=(ALL) ALL\n"
                              "%wheel ALL=(ALL) NOPASSWD: ALL")))
  (services
   (let ((base
          (append
           (list
            (service bluetooth-service-type
                     (bluetooth-configuration
                      (auto-enable? #t)))
            (service cups-service-type
                     (cups-configuration
                      (web-interface? #t)
                      (extensions
                       (list cups-filters))))
            (service docker-service-type)
            (service gnome-desktop-service-type)
            (service guix-home-service-type
                     `(("mathieu" ,my-home)))
            (service openssh-service-type)
            (service pcscd-service-type)
            (service qemu-binfmt-service-type
                     (qemu-binfmt-configuration
                      (platforms (lookup-qemu-platforms "arm" "aarch64"))))
            (service special-files-service-type
                     `(("/bin/sh" ,(file-append bash "/bin/sh"))
                       ("/bin/bash" ,(file-append bash "/bin/bash"))
                       ("/usr/bin/perl" ,(file-append perl "/bin/perl"))
                       ("/usr/bin/env" ,(file-append coreutils "/bin/env"))))
            (service syncthing-service-type
                     (syncthing-configuration (user "mathieu")))
            (service tlp-service-type (tlp-configuration
                                       (wol-disable? #f)
                                       (usb-autosuspend? #f)
                                       (usb-blacklist "0bda:8156")))
            (udev-rules-service
             'uart
             (udev-rule
              "90-misc.rules"
              (string-append "SUBSYSTEM==\"tty\", ATTRS{idVendor}==\"067b\","
                             "ATTRS{idProduct}==\"2303\", SYMLINK+=\"db-9\""
                             "TAG+=\"uaccess\", SYMLINK+=\"db-9\""
                             "\n"
                             "SUBSYSTEM==\"tty\", ATTRS{idVendor}==\"0403\","
                             "ATTRS{idProduct}==\"6001\","
                             "TAG+=\"uaccess\", SYMLINK+=\"rs232\""
                             "\n"
                             "SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"1fc9\","
                             "ATTRS{idProduct}==\"014e\", MODE=\"0777\","
                             "TAG+=\"uaccess\""
                             "\n"
                             "SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"1fc9\","
                             "ATTRS{idProduct}==\"0152\", MODE=\"0777\","
                             "TAG+=\"uaccess\"")))
            (udev-rules-service 'rtl-sdr rtl-sdr)
            (service wireguard-service-type
                     (wireguard-configuration
                      (addresses (list "10.10.10.2/24"))
                      (dns (list "10.10.10.3" "192.168.1.254"))
                      (peers
                       (list
                        (wireguard-peer
                         (name "viso")
                         (endpoint "othacehe.org:443")
                         (public-key "vC91Z9lHP/9RzFdzlXm3qbVMBmtEp5yp/hEwqXTteGw=")
                         (allowed-ips '("10.0.0.0/8"))
                         (keep-alive 25))))))
            (set-xorg-configuration
             (xorg-configuration
              (keyboard-layout keyboard-layout))))
           %desktop-services)))
     (modify-services base
       (guix-service-type
        config =>
        (guix-configuration
         (inherit config)
         (discover? #t)
         (substitute-urls
          (append (list "https://substitutes.nonguix.org")
                  %default-substitute-urls))
         (authorized-keys
          (append (list
                   (local-file "./keys/elbruz.scm")
                   (local-file "./keys/nonguix.scm")
                   (local-file "./keys/frmonpc1176.scm")
                   (local-file "./keys/frmonpc1177.scm"))
                  %default-authorized-guix-keys))))
       (gdm-service-type
        config =>
        (gdm-configuration
         (auto-login? #t)
         (default-user "mathieu")
         (wayland? #t)))
       (delete pulseaudio-service-type))))
  (bootloader
   (bootloader-configuration
    (bootloader grub-efi-bootloader)
    (targets '("/boot/efi"))
    (keyboard-layout keyboard-layout)))
  (swap-devices
   (list
    (swap-space
     (target
      (uuid "a755181e-c6c7-45c3-ac7a-5f882961d866")))))
  (file-systems
   (cons* (file-system
            (mount-point "/boot/efi")
            (device (uuid "3E1A-D839" 'fat32))
            (type "vfat"))
          (file-system
            (mount-point "/")
            (device
             (uuid "e70ffaa2-31f6-4721-a9e7-382a60c0a921"
                   'ext4))
            (type "ext4"))
          %base-file-systems)))
