#!/bin/sh
set -e
set -x
ISO="$(./pre-inst-env guix system disk-image --file-system-type=iso9660 gnu/system/install.scm --verbosity=3)"
qemu-img create -f qcow2  /tmp/t.img 10G

cp "$(./pre-inst-env guix build ovmf)/share/firmware/ovmf_x64.bin" /tmp
chmod +w /tmp/ovmf_x64.bin
EFI_OPTS="-bios $(./pre-inst-env guix build ovmf)/share/firmware/ovmf_x64.bin"

cp $ISO /tmp/img
chmod +w /tmp/img
exec qemu-system-x86_64 -enable-kvm $EFI_OPTS -cdrom $ISO -drive file=/tmp/t.img,format=qcow2 -m 1024 -net user -net nic,model=virtio -no-reboot -boot c
