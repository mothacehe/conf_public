#!/bin/sh

TMP_FILE=/tmp/patch

cp /dev/stdin $TMP_FILE
BUG_NUMBER=$(grep -oP 'bug#\K[0-9]+' $TMP_FILE)
PATCH_NUMBER=$(grep  -oP 'PATCH.*?\K[0-9]+/[0-9]+' $TMP_FILE | cut -f1 -d"/")

if [ -z "$BUG_NUMBER" ]; then
    PATCH_NUMBER=0
elif [ "$PATCH_NUMBER" -eq "0" ]; then
    exit 0
fi

PATCH_DIR=~/News/patches/$BUG_NUMBER
PATCH_FILE=$PATCH_DIR/$PATCH_NUMBER.patch
mkdir -p $PATCH_DIR
cp $TMP_FILE $PATCH_FILE

echo "Write $PATCH_FILE."
