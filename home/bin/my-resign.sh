#!/bin/bash

guix shell -C -F openssl@3 -D guix --share=/tmp --share=/home/mathieu -- ~/dc-fr/sources/meta-application-fr/scripts/resign.sh ~/dc-fr/build/tmp/deploy/images/cr5-g1/dc-image-final-cr5-g1.landis-modules.tar.gz ~/work/landis/src/image-tools/enteteK ~/dc-fr/sources/meta-application-fr/keys/AcSignaturePrim.key.pem ~/dc-fr/sources/meta-application-fr/keys/signatureLogicielle.pub.pem
