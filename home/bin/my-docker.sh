#!/bin/sh

DOCKER_IMAGE=$1

docker run --privileged --rm -it --net=host \
    -v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK \
    -v ~/.ssh:/home/$USER/.ssh/ \
    -v ~/.gitconfig:/home/$USER/.gitconfig \
    -v ~/.git-credentials:/home/$USER/.git-credentials \
    -v ~/.bashrc:/home/$USER/.bashrc \
    -v ~/.bash_history:/home/$USER/.bash_history \
    -v ~/.bash_aliases:/home/$USER/.bash_aliases \
    -v /dev:/dev \
    -v /run:/run \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/group:/etc/group:ro \
    -v /etc/resolv.conf:/etc/resolv.conf \
    -v /opt:/opt \
    -v ~/.local:/home/$USER/.local \
    --user $(id -u):$(id -g) \
    -e DISPLAY=$DISPLAY \
    -e SSH_AUTH_SOCK=$SSH_AUTH_SOCK -v ${PWD}:${PWD} --workdir ${PWD} \
    -e TERM=$TERM \
    $DOCKER_IMAGE
