#!/bin/sh

IPMI_TOOL=ipmi-power
IPMI_ARGS="-h 82.64.160.236 -u admin -p $(pass home/elbruz)"
IPMI_CMD=""

case $1 in
    "on")
	IPMI_CMD="--on";;
    "off")
	IPMI_CMD="--off";;
    "status")
	IPMI_CMD="--stat";;
    *)
	echo "Usage $0 on | off | status";;
esac

if [ -n "$IPMI_CMD" ]; then
    $IPMI_TOOL $IPMI_ARGS $IPMI_CMD
fi
