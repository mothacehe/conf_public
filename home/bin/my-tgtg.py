#!/usr/bin/python3

import requests
import pickle
from tgtg import TgtgClient
from datetime import datetime

users = [{"email": "mothacehe@gmail.com",
          "password": "s3FeGj8a4Ow0Pcii",
          "channel": "kRheTuuypPWRznEm"}]

notify_url = "https://notify.run/"
cache_file = "/tmp/tgtg.data"
timeout = 3600

try:
    with open(cache_file, 'rb') as handle:
        cache = pickle.load(handle)
except FileNotFoundError:
    cache = {}

for user in users:
    client = TgtgClient(email=user['email'], password=user['password'])
    print("Client " + user['email'])
    for item in client.get_items():
        name = item['display_name']
        available = item['items_available']
        item_id = item['item']['item_id']
        if available > 0 and not(item_id in cache.keys()):
            message = "Paniers disponibles chez " + name
            channel = notify_url + user['channel']
            requests.post(channel, data = message)
            cache[item_id] = datetime.now()
        else:
            print("Nothing available at " + name)

for item in list(cache):
    diff = (datetime.now() - cache[item]).total_seconds()
    if  diff > timeout:
        print("Removing expired entry, diff = " + str(diff))
        cache.pop(item)
        
with open(cache_file, 'wb') as handle:
    pickle.dump(cache, handle)
