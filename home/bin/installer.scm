(use-modules (gnu installer)
             (guix))

(run-with-store (open-connection)
  (mlet* %store-monad ((drv
                        (lower-object (installer-program)))
                       (out -> (derivation->output-path drv)))
    (mbegin %store-monad
      (built-derivations (list drv))
      (return (format #t "~a~%" out)))))
