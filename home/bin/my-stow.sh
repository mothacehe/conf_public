#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

sudo -E -u mathieu ~/.guix-profile/bin/stow -d /home/mathieu/conf/home -t /home/mathieu . -R
